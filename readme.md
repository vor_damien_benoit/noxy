##Noxy :
Noxy (**NO**de pro**XY** ) is a small proxy server allowing to use http-proxy with a JSON config file.

It allow for example to be used as a front proxy routing apps beetwen node.js, apache and nginx and support websockets.

NOXY: 80 > Apache:8080 | node:1337

installation : ``` npm install noxy -g ```

Usage :

``` noxy --help ```

``` noxy -c myrouting.conf.json ```


Example configuration :
```javascript
/**
* here is an example configuration for noxy (NOde proXY)
*/
{
	"server" : {
		"hostname"	: "" // empty string to bind on everything
		,"port"		: 5050
	}
	,"domains" : {
		"dev.adive.in"	: { "target" : "http://127.0.0.1:1337" }
		,"default"		: { "target" : "http://127.0.0.1:80" }
	}
}
```
